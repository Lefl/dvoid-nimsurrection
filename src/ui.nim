import nico
import noisy
import vectors
from entities import Entity

const StarSlowdown = 0.125
const StarPercentage = 0.75 # At what noise value should a star be shown?
# const StarColors = [7, 9, 10, 12, 15]
var simplex = initSimplex(2802)
simplex.octaves = 3
simplex.frequency = 0.25
simplex.amplitude = 1.75
simplex.lacunarity = 2

proc camSize(): Vector =
  Vector(x: screenWidth.float, y: screenHeight.float)


proc drawBackground(playerPos: Vector) =
  let playerPosition = playerPos * StarSlowdown
  var starRangeX = (playerPosition.x - screenWidth / 2).int .. (playerPosition.x + screenWidth / 2).int
  var starRangeY = (playerPosition.y - screenWidth / 2).int .. (playerPosition.y + screenWidth / 2).int

  setColor(7)
  for x in starRangeX:
    for y in starRangeY:
      let noiseValue = simplex.value(x, y)
      if noiseValue > StarPercentage:
        var starPosition = Vector(x: x.float, y: y.float)
        starPosition = starPosition.toView(playerPosition, Vector(x: screenWidth.float, y: screenHeight.float))
        pset(starPosition.x, starPosition.y)


proc drawPlayerHealth(percentage: float) =
  setColor(3) # White
  var boxWidth = screenWidth / 3
  let boxHeight = 16
  # Boxes
  boxfill(16, screenHeight - 8 - boxHeight, boxWidth, boxHeight)
  setColor(5)
  boxfill(18, screenHeight - 6 - boxHeight, boxWidth - 4, boxHeight - 4)
  # Actual Health
  boxWidth = (screenWidth / 3 - 4) * percentage
  setColor(11) # Green
  boxfill(18, screenHeight - 6 - boxHeight, boxWidth, boxHeight - 4)


proc drawPlayerShields(percentage: float) =
  setColor(1) # Dark Blue
  let boxWidth = (screenWidth / 3 - 4) * percentage
  let boxHeight = 16
  boxfill(18, screenHeight - 6 - boxHeight, boxWidth, (boxHeight - 4) / 2)


proc drawPlayerInformation(player:  Entity) =
  drawPlayerHealth(player.health / player.maxHealth)
  drawPlayerShields(player.shields / player.maxShields)


proc drawEnemyInformation(enemy: Entity) =
  setColor(8)
  printc(enemy.name, screenWidth div 2, 16)
  #Boxes:
  setColor(2)
  boxfill(10, 32, screenWidth - 32, 16)
  setColor(5)
  boxfill(12, 34, screenWidth - 36, 12)
  # Health
  let healthPercent = enemy.health / enemy.maxHealth
  let shieldsPercent = enemy.shields / enemy.maxShields
  setColor(8)
  boxfill(12, 34, screenWidth - 36 * healthPercent, 12)
  # Shields
  setColor(1)
  boxfill(12, 34, screenWidth - 36 * shieldsPercent, 6)


proc drawEnemies*(enemies: seq[Entity], camOffset: Vector) =
  for e in enemies:
    var viewEnemyPos = e.position.toView(camOffset, camSize())
    spr(e.rotation, viewEnemyPos.x - 16, viewEnemyPos.y - 16)


proc drawOriginIndicator(playerPosition: Vector) =
  setColor(1)
  var toOrigin = Vector() - playerPosition
  toOrigin.x += screenWidth / 2
  toOrigin.y += screenHeight / 2
  var excessDistance = Vector()
  if toOrigin.x < 0:
    excessDistance.x = abs(toOrigin.x)
    toOrigin.x = 0
  elif toOrigin.x > screenWidth:
    excessDistance.x = toOrigin.x - screenWidth.float
    toOrigin.x = screenWidth.float
  if toOrigin.y < 0.0:
    excessDistance.y = abs(toOrigin.y)
    toOrigin.y = 0
  elif toOrigin.y > screenHeight:
    excessDistance.y = toOrigin.y - screenHeight.float
    toOrigin.y = screenHeight.float

  if len(excessDistance) > 256:
    if toOrigin.x > 0:
      line(screenWidth - 1, toOrigin.y - 10, screenWidth - 1, toOrigin.y + 10)
    else:
      line(0, toOrigin.y - 10, 0, toOrigin.y + 10)
    if toOrigin.y > 0:
      line(toOrigin.x - 10, screenHeight - 1, toOrigin.x + 10, screenHeight - 1)
    else:
      line(toOrigin.x - 10, 0, toOrigin.x + 10, 0)


proc drawPlayerOutOfBounds*() =
  setColor(8)
  printc("Return to Battlezone", screenWidth / 2, screenHeight / 2)
  box(screenWidth / 2 - 50, screenHeight / 2 - 20, 100, 40)


proc drawMain*(player: var Entity, enemies: var seq[Entity]) =
  drawBackground(player.position)
  drawPlayerInformation(player)
  drawEnemies(enemies, player.position)
  spr(player.rotation, screenWidth / 2 - 16, screenHeight / 2 - 16)
  drawOriginIndicator(player.position)


proc drawMenu*(selectedPosition: int) =
  setColor(3)
  box(screenWidth / 2 - 75, screenHeight / 2 - 128, 150, 256)
  setColor(0)
  boxfill(screenWidth / 2 - 74, screenHeight / 2 - 127, 148, 254)

  let margin = 5
  let menuRect = [screenWidth / 2 - 74 + margin, screenHeight / 2 - 127 + margin, 148 - margin * 2, (254 - margin * 4) / 3]
  let menuText = ["Continue", "Credits", "Exit"]
  for i in 0 .. 2:
    if selectedPosition == i: setColor(3) else: setColor(7)
    let boxPositionY = menuRect[1] + (menuRect[3] + margin) * i
    let textPositionX = (menuRect[0] + menuRect[2] / 2).float
    let textPositionY = (boxPositionY + menuRect[3] / 2).float
    box(menuRect[0], boxPositionY, menuRect[2], menuRect[3])
    printc(menuText[i], textPositionX,textPositionY)
