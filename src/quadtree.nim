import vectors
import nico
import entities
from sequtils import concat


const MaxPoints = 4 # How many points there ccan be in one quad before it's divided

type Quad = ref object
  position*: Vector
  dimensions: Vector
  divided: bool
  depth: int
  nodes*: seq[Entity]
  topRight*: Quad
  botRight*: Quad
  botLeft: Quad
  topLeft*: Quad

# Forward declaration
method insert*(this: Quad, entity: Entity): bool {.base.}

proc newQuad*(p, dim: Vector, depth: int): Quad =
  result = Quad(position: p, dimensions: dim, depth: depth)
  result.divided = false


method divide(this: Quad) {.base.} =
  let newDimensions = this.dimensions / 2
  this.topLeft = newQuad(this.position, newDimensions, this.depth + 1)

  block topRight:
    var trPos = this.position
    trPos.x += newDimensions.x
    this.topRight = newQuad(trPos, newDimensions, this.depth + 1)
  block bottomLeft:
    var blPos = this.position
    blPos.y += newDimensions.y
    this.botLeft = newQuad(blPos, newDimensions, this.depth + 1)
  block bottomRight:
    var blPos = this.position
    blPos = blPos + newDimensions
    this.botRight = newQuad(blPos, newDimensions, this.depth + 1)

  for i in countdown(len(this.nodes) - 1, 0):
    var entity = this.nodes[i]
    if this.topLeft.insert(entity) or this.topRight.insert(entity) or
        this.botLeft.insert(entity) or this.botRight.insert(entity):
      this.nodes.del(i)
  this.divided = true


method insert*(this: Quad, entity: Entity): bool {.base.} =
  # Check if in bounds, if not return false
  let lowerRightBound = this.position + this.dimensions
  let collisionVerteces = calculateVerteces(
                                            entity.position,
                                            entity.rotation.float * RotationStep,
                                            entity.collisionShape
                                            )
  for v in collisionVerteces:
    if not (v.x >= this.position.x and v.x <= lowerRightBound.x and
      v.y >= this.position.y and v.y <= lowerRightBound.y):
      return false

  # If there's still enough space in the current quad, put the new node into it.
  if not this.divided and len(this.nodes) < MaxPoints:
    this.nodes.add(entity)
    return true

  # If there's no more space, divide it
  if not this.divided:
    this.divide()

  result = this.topLeft.insert(entity) or this.topRight.insert(entity) or
                          this.botLeft.insert(entity) or this.botRight.insert(entity)
  # We don't really want to but this node doesn't fit anywhere else
  if not result:
    this.nodes.add(entity)
    result = true


method undivide(this: Quad) {.base.} =
  let childNodeAmount = len(this.topLeft.nodes) + len(this.topRight.nodes) +
                        len(this.botLeft.nodes) + len(this.botRight.nodes)
  if childNodeAmount < MaxPoints:
    this.nodes = concat(
                        this.topLeft.nodes,
                        this.topRight.nodes,
                        this.botLeft.nodes,
                        this.botRight.nodes
                        )
    this.divided = false
    this.topLeft = nil
    this.topRight = nil
    this.botLeft = nil
    this.botRight = nil


method remove*(this: Quad, entity: Entity): bool {.base.} =
  result = false
  for i in 0..len(this.nodes) - 1:
    if this.nodes[i] == entity:
      this.nodes.del(i)
      result = true
      break

  if this.divided:
    result = this.topLeft.remove(entity) or this.topRight.remove(entity) or
              this.botLeft.remove(entity) or this.botRight.remove(entity)
    this.undivide()


method queryClosebys*(this: Quad, entity: Entity, encounteredEntities: var seq[Entity]): bool {.base.} =
  if this.divided:
    if this.topLeft.queryClosebys(entity, encounteredEntities) or
      this.topRight.queryClosebys(entity, encounteredEntities) or
      this.botLeft.queryClosebys(entity, encounteredEntities) or
      this.botRight.queryClosebys(entity, encounteredEntities):
      result = true
  elif not this.divided:
    for node in this.nodes:
      if node == entity:
        result = true
        break
  if result:
    for node in this.nodes:
      encounteredEntities.add(node)


method draw*(this: Quad, camOffset: Vector) {.base.} =
  setColor(7)
  let b = this.position.toView(camOffset, Vector(x: screenWidth.float, y: screenHeight.float))
  box(b.x, b.y, this.dimensions.x, this.dimensions.y)
  if this.divided:
    this.topLeft.draw(camOffset)
    this.topRight.draw(camOffset)
    this.botLeft.draw(camOffset)
    this.botRight.draw(camOffset)
