import math


type Vector* = object
  x*: float
  y*: float


proc `+`*(a, b: Vector): Vector =
  result = a
  result.x += b.x
  result.y += b.y


proc `-`*(a, b: Vector): Vector =
  result = a
  result.x -= b.x
  result.y -= b.y


proc `*`*(a, b: Vector): Vector =
  result = a
  result.x *= b.x
  result.y *= b.y


proc `*`*(a: Vector, b: float): Vector =
  result = a
  result.x *= b
  result.y *= b


proc `/`*(a: Vector, b: float): Vector =
  result = a
  result.x /= b
  result.y /= b


proc len*(a: Vector): float =
  sqrt(pow(a.x, 2) + pow(a.y, 2))


proc norm*(a: Vector): Vector =
  result = a / len(a)

method toView*(this, camOffset, camSize: Vector): Vector {.base.} =
  var screenCorner = Vector(
    x: camOffset.x - camSize.x / 2,
    y: camOffset.y - camSize.y / 2
  )
  result = this - screenCorner


method toWorld*(this, camOffset, camSize: Vector): Vector {.base.} =
  var screenCorner = Vector(
    x: camOffset.x - camSize.x / 2,
    y: camOffset.y - camSize.y / 2
  )
  result = screenCorner + this
